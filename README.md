# Veloren Coding Challenges

Hello! These are the Veloren Coding Challenges. The objective of these programming challenges is to get new Veloren programmers comfortable with the engine. The challenges are created by @AngelOnFira, with writing assistance from @Kidsnextdorks.

## What skills do you need?

These challenges assume you have basic programming knowledge from a different language. It will ask you to complete functions, read and understand already written code, and deal with data structures, math, and other programming concepts.

If you find that these challenges are too involved, we highly recommend you to learn from the Rust book or other programming tutorials. If you spend the time it takes to learn, you can get there!

To start, read the readme in the first challenge folder. The challenges are currently being made by @AngelOnFira, with writing help from @Kidsnextdorks.