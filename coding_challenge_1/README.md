# Veloren Coding Challenge 1

Hello! This is the first Veloren coding challenge. The objective of these programming challenges is to get new Veloren programmers comfortable with the engine. The challenge was created by @AngelOnFira, with writing assistance from @Kidsnextdorks.

## What skills do you need?

These challenges assume you have basic programming knowledge from a different language. It will ask you to complete functions, read and understand already written code, and deal with data structures, math, and other programming concepts.

If you find that these challenges are too involved, we highly recommend you to learn from the Rust book or other programming tutorials. If you spend the time it takes to learn, you can get there!

## The challenge

<img src="https://cdn.discordapp.com/attachments/444005079410802699/567174041069420544/fantasy_map_1555295102766.png"/>

It is a dark time in Bagh Maldir. Over the last few years, famine has spread across the kingdom. Civil unrest is growing, as the population dwindles. The royal council has reached the end of their options. However, today is their lucky day.

You are Iefyr, a traveling wizard. You have no home and have been banished from many other kingdoms for your use of black magic. However, you feel that it is time for a change. Today happens to be the day that you find yourself in this new kingdom. To them, you are no one. You have cast many shapeshifting spells on yourself as to not cause immediate terror upon your arrival.

Upon seeing a wizard entering the castle gates, the guards quickly bring you to the throne room. It has been a while since the last wizard has come through this kingdom, and the king is quick to explain the problems to you.

With the level of civil unrest so high, many of the people have stopped working. You see that there are many that would be good candidates to work as farmers. You now need to calculate which ones will be able to work, as well as how many workers there are total.

There are 2141 people in the kingdom. The king wants to put them all to work but you think it would be better to only make certain people work. Some of the population is too old, and some are too young. To determine which ones cannot work, they must meet one of the following requirements;

- Every sixth person is too young, and can’t work
- Every eighth person is too old, and can’t work
- Every 113th person is noble, and they refuse to work.

However, if any citizen meets more than one of these conditions, then they are a false negative and can work. For example, a citizen who is too young and too old has had their information gathered incorrectly, and is actually able to work.

<hr>

To start, open `coding_challenge_1/src/main.rs`. There are basic functions there for you to work inside of. Each is meant to be template code. You may remove it if you see fit, however, you should be able to solve the problem in these functions. If you need help, feel free to ask questions in the #learning channel on the Veloren Discord. You can also take a look at other solutions, as well as post your own [here](https://www.reddit.com/r/Veloren/comments/bdvyxa/veloren_coding_challenge_1_solutions/)

Best of luck!