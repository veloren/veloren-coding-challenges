## The challenge

<img src="https://cdn.discordapp.com/attachments/444005079410802699/567174041069420544/fantasy_map_1555295102766.png"/>

<hr>

You have just calculated the Harmonic Convergences of each day, and have found that there are three days with the same greatest GHC, the 52nd, 53rd, and 54th day. As you figure out what day these are, you realize that today is the 54th day! Knowing that you have very little time to spare, you run down to a plot of farmland that has been prepared for you. This land has been tilled to prepare for normal crops, but the three beans that you have should be planted in the most fertile soil.

You are able to calculate the fertility of the soil for each meter squared of land. You need to plant the beans so that they have the highest fertility they can. The farm is 20m x 20m, so there are 400 possible plots. To get the fertility of a plot, you must use the (x, y) coordinates in the provided `get_fertility` function.

There are more to the beans, however. Beans don't only need their plot to be fertile. These magical beans have roots that spread deep underground and gain fertility from nearby plots. Any plot that is 7 away from the plot the bean is planted in also counts towards its fertility score. Distance is calculated with the [Manhattan Distance](https://www.quora.com/What-is-Manhattan-Distance).

```
|..........|
|..........|
|..R.......|
|.RRR......|
|RRBRR.....|
|.RRR......|
|..R.......|
```

> You can see the bean (B) surrounded by its roots (R) that are up to a distance of 2 away from it.

Also note, if the roots of two or more beans overlap in a plot, the fertility only counts once.

You must figure out where to plant the beans so that you get the highest fertility score of all three beans scores added together. And you must act fast! Time is not on your side.

<hr>

To start, open `coding_challenge_3/src/main.rs`. There is some template code there for you to work with. Each is meant to be template code. You may remove it if you see fit, however, you should be able to solve the problem in these functions. If you need help, feel free to ask questions in the #learning channel on the Veloren Discord. You can also take a look at other solutions, as well as post your own [here](https://www.reddit.com/r/Veloren/comments/bix3zw/veloren_coding_challenge_3_solutions/)

Best of luck!