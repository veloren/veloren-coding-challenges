use noise::{NoiseFn, Perlin};

fn main() {

}

fn get_fertility(x: f64, y: f64) -> f64 {
    let perlin = Perlin::new();
    perlin.get([x / 10.0, y / 10.0]) * 100.0
}