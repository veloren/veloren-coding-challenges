## The challenge

<img src="https://cdn.discordapp.com/attachments/444005079410802699/567174041069420544/fantasy_map_1555295102766.png"/>

You have spent a full day on your preliminary calculations. The commoners are still excited that you have arrived, however, some of them are getting angry that you haven't taken any visible action yet. As you walked around the kingdom to complete your first calculations, you heard some angry remarks from the crowd, like "fizzbuzz you old hag".

Luckily, the second part of the preparation shouldn't take long to complete. You must only prepare a spell. This spell will help you find the best day to plant your crops. The spell is needed because the seeds you have don't produce normal crops. You have a few beans that you won from a game of Knaps a few taverns back. You knew right away that they were enchanted, and as you are an experienced wizard, you know that there are rules to planting these seeds.

You must create an incantation to find the right day to plant the enchanted seeds. You must follow the spell closely, as you will be altering ancient recorded numbers, and improper use of these numbers could result in terrible destruction. Your goal is to find the day with the greatest harmonic convergence (GHC). The day will be relative to the spring equinox, which was used to find the ancient recordings. These numbers will be provided to you.

First, you must shift the ancient numbers to match the location of Bagh Maldir. The kingdom is 413 km north, 212 km east of the original recording. The calculation as follows;

- If the number is greater than 500, then the number should be multiplied by the distance north added to the distance east.
- If the number is less than 500, then the number should be multiplied by the distance north multiplied to the distance east.
- If the number is 500, then it should not be adjusted

After the shift, the number should change to the remainder when divided by 1000

Second, all of the non-pure numbers need to be removed. Pure numbers are as follows;

- Must be even
- Must be at most 400 away from 500

Now, incantations cannot be cast without proper procedure. They must be done with meticulous accuracy, and in the correct order. All of them must make use of the Rust `iter()`. The specific functions to be used are as follows;

- The first part, location shifting, must be done using the map function
- The second part, removing non-pure numbers, must be done using the filter function

Now that the ancient recordings have been calibrated, it's time to find the point of greatest harmonic convergence. This occurs when 5 consecutive numbers have the greatest variance. More specifically, the set of 5 numbers with the biggest difference between its max number and min number. For example, [1, 2, 5, 4, 3] will only have a harmonic convergence of 4 (5 - 1), whereas [1, 2, 3, 10, 8] will be 9 (10 - 1).

Each number represents a day. You must find the day which is the beginning of the set for the GHC.

<hr>

To start, open `coding_challenge_2/src/main.rs`. There is some template code there for you to work with. Each is meant to be template code. You may remove it if you see fit, however, you should be able to solve the problem in these functions. If you need help, feel free to ask questions in the #learning channel on the Veloren Discord. You can also take a look at other solutions, as well as post your own [here](https://www.reddit.com/r/Veloren/comments/bg8er9/veloren_coding_challenge_2_solutions/)

Best of luck!